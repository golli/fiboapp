/** @format */

import {AppRegistry} from 'react-native';
import App from './src/main';
AppRegistry.registerComponent('fiboApp', () => App);
