/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ActivityIndicator,
    FlatList,
    StatusBar
}from 'react-native'
import SplashScreen from 'react-native-splash-screen';
import AppBodyData from './appBodyData'
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {

    constructor(props){
      super(props);
      this.state ={
          isLoading: true,
          dataSource:null,
      }
    }


    componentDidMount(){
        SplashScreen.hide();
        return fetch('http://10.0.2.2:3000/getFibonacchi')
            .then((response)=>response.json())
            .then((responseJson)=>{

               this.setState({
                  isLoading: false,
                  dataSource: responseJson,
        }, function(){

        });
            }).catch((error)=>{
                console.error(error);
            });
    }

  render() {

     if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
 console.log(this.state.dataSource);
    return(

      <AppBodyData data={this.state.dataSource}/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4f6d7a',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    margin: 10,
    color:'#F5FCFF',
  },
  instructions: {
    textAlign: 'center',
    color: '#F5FCFF',
    marginBottom: 5,
  },
});
