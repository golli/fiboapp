/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    StatusBar
}from 'react-native'
export default class AppBodyData extends Component{
render(){
            let numbers=this.props.data.numbers.map(function (value,key) {
                return(
                        <Text style={styles.instructions}>{value}</Text>)
            })
    return(
      <ScrollView>
      <View style={styles.container}>
        <Text style={styles.welcome}>{this.props.data.title}</Text>
        <Text style={styles.instructions}>{this.props.data.Description}</Text>
          {numbers}
          <Text style={styles.instructions}>And the sum of even numbers is :</Text>
        <Text style={styles.instructions}>{this.props.data.sumEven}</Text>

      </View>
      </ScrollView>
      );

    }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

module.export = AppBodyData;