/*** src/main.js ***/

import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { createAppContainer } from 'react-navigation';
import Fibonacci from './screens/appBodyData.js';
import Welcome from './screens/Welcome.js';



const AppNavigator  = createStackNavigator({
  Welcome: { screen: Welcome },
  Fibonacci: { screen: Fibonacci },

});
const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
